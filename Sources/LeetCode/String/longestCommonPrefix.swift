//
//  File.swift
//  
//
//  Created by kemchenj on 2020/1/30.
//

class LongestCommonPrefix {
    func longestCommonPrefix(_ strs: [String]) -> String {
        var result = ""
        var iter = strs.makeIterator()
        if let first = iter.next() {
            result = first
            while let next = iter.next() {
                result = longestCommonPrefix(lhs: result, rhs: next)
            }
        }
        return result
    }

    func longestCommonPrefix(lhs: String, rhs: String) -> String {
        var result = ""
        var lhsCursor = lhs.startIndex
        var rhsCursor = rhs.startIndex

        while lhsCursor != lhs.endIndex, rhsCursor != rhs.endIndex {
            guard lhs[lhsCursor] == rhs[rhsCursor] else { return result }

            result.append(lhs[lhsCursor])
            lhs.formIndex(after: &lhsCursor)
            rhs.formIndex(after: &rhsCursor)
        }

        return result
    }
}
