/// 资料链接： https://www.cnblogs.com/1996swg/p/7116017.html

class StrStr {
    
    func bruteForce(_ haystack: String, needle: String) -> Int {
        guard !haystack.isEmpty || !needle.isEmpty else { return 0 }

        var index = haystack.startIndex
        let endIndex = haystack.endIndex

        if haystack.count >= needle.count {
            while index < haystack.endIndex, haystack.distance(from: index, to: endIndex) > needle.count {
                var haystackIndex = index
                var needleIndex = needle.startIndex

                while true {
                    guard needleIndex != needle.endIndex else {
                        return haystack.distance(from: haystack.startIndex, to: index)
                    }
                    guard haystack[haystackIndex] == needle[needleIndex] else { break }

                    haystack.formIndex(after: &haystackIndex)
                    needle.formIndex(after: &needleIndex)
                }

                haystack.formIndex(after: &index)
            }
        }

        return -1
    }

    public func rk(_ haystack: String, _ needle: String) -> Int {
        guard let first = needle.first else { return 0 }
        let suffix = needle.dropFirst()
        let suffixHash = suffix.hashValue

        let endIndex = haystack.endIndex
        var i = haystack.startIndex
        guard
            var suffixWindowLeft = haystack.index(i, offsetBy: 1, limitedBy: endIndex),
            var suffixWindowRight = haystack.index(suffixWindowLeft, offsetBy: suffix.count, limitedBy: endIndex)
            else {
                return -1
        }

        while suffixWindowRight != endIndex {
            let window = haystack[suffixWindowLeft..<suffixWindowRight]

            if first == haystack[i], window.hashValue == suffixHash, window == suffix {
                return haystack.distance(from: haystack.startIndex, to: i)
            }

            haystack.formIndex(after: &i)
            haystack.formIndex(after: &suffixWindowLeft)
            haystack.formIndex(after: &suffixWindowRight)
        }

        return -1
    }

    /// 失配时，模式串向右移动的位数为：已匹配字符数 - 失配字符的上一位字符所对应的最大长度值
    func kmp(_ haystack: String, _ needle: String) -> Int { 0 }

    func ckt(_ str: String) -> [Int] {
        guard str.count > 0 else { return [] }
        var next = [Int](repeating: 0, count: str.count)
        next[0] = -1

        var strArr = Array(str)

        var p = -1
        var s = 0

        while s < (strArr.count - 1) {
            if p == -1 || strArr[p] == strArr[s] {
                p += 1
                s += 1
                next[s] = p
            } else {
                p = next[p]
            }
        }

        return next
    }

    func bm(_ haystack: String, _ needle: String) -> Int { 0 }

    func bmh(_ haystack: String, _ needle: String) -> Int { 0 }

    func sunday(_ haystack: String, _ needle: String) -> Int { 0 }
}
