//
//  File.swift
//  
//
//  Created by kemchenj on 2020/1/30.
//

class Atoi {
    
    func myAtoi(_ str: String) -> Int {
        var numStr = str.drop(while: { $0 == " " })
        var sign: Character = "+"
        if let _sign = numStr.first, ["+", "-"].contains(_sign) {
            sign = _sign
            numStr = numStr.dropFirst()
        }
        numStr = numStr.prefix(while: { $0.isNumber })
        guard !numStr.isEmpty else { return 0 }
        guard let num = Int("\(sign)\(numStr)") else {
            return sign == "+" ? Int(Int32.max) : Int(Int32.min)
        }
        if num > Int(Int32.max) {
            return Int(Int32.max)
        } else if num < Int(Int32.min) {
            return Int(Int32.min)
        } else {
            return num
        }
    }
}
