//
//  atoiTests.swift
//  
//
//  Created by kemchenj on 2020/1/30.
//

import XCTest
@testable import LeetCode

final class AtoiTests: XCTestCase {
    func testAtoi() {
        XCTAssertEqual(0, Atoi().myAtoi("wjksdaf123"))
        XCTAssertEqual(0, Atoi().myAtoi("wjksdaf-123"))
        XCTAssertEqual(0, Atoi().myAtoi("wjksdaf+123"))
        XCTAssertEqual(123, Atoi().myAtoi("    123"))
        XCTAssertEqual(123, Atoi().myAtoi("123"))
        XCTAssertEqual(-123, Atoi().myAtoi("-123"))
        XCTAssertEqual(123, Atoi().myAtoi("+123"))
        XCTAssertEqual(12312, Atoi().myAtoi("12312jasdklfjakf"))
        XCTAssertEqual(-12312, Atoi().myAtoi("-12312jasdklfjakf"))
        XCTAssertEqual(12312, Atoi().myAtoi("+12312jasdklfjakf"))
        XCTAssertEqual(Int(Int32.min), Atoi().myAtoi("-91283472332"))
        XCTAssertEqual(Int(Int32.max), Atoi().myAtoi("20000000000000000000"))
        XCTAssertEqual(3, Atoi().myAtoi("3.141578"))
    }
}

