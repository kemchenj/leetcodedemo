//
//  longestCommonPrefixTests.swift
//  
//
//  Created by kemchenj on 2020/1/30.
//

import XCTest
@testable import LeetCode

final class LongestCommonPrefixTests: XCTestCase {
    func testLongestCommonPrefix() {
        XCTAssertEqual("", LongestCommonPrefix().longestCommonPrefix(["abc", "cba"]))
        XCTAssertEqual("", LongestCommonPrefix().longestCommonPrefix(["123", "", "12345"]))
        XCTAssertEqual("12", LongestCommonPrefix().longestCommonPrefix(["123", "12", "12345"]))
        XCTAssertEqual("123", LongestCommonPrefix().longestCommonPrefix(["123"]))
    }
}

