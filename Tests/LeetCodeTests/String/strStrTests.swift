//
//  strStrTests.swift
//
//
//  Created by kemchenj on 2020/1/30.
//

import XCTest
@testable import LeetCode

final class StrStrTests: XCTestCase {
    func testBruteForce() {
        strStr(impl: StrStr().bruteForce)
    }

    func testRK() {
        strStr(impl: StrStr().rk)
    }

    // func testKMP() {
    //     strStr(impl: StrStr().kmp)
    // }

    // func testBM() {
    //     strStr(impl: StrStr().bm)
    // }

    // func testBMH() {
    //     strStr(impl: StrStr().bmh)
    // }

    // func testSunday() {
    //     strStr(impl: StrStr().sunday)
    // }

    func strStr(impl: (String, String) -> Int) {
        XCTAssertEqual(0, impl("", ""))
        XCTAssertEqual(0, impl("a", ""))
        XCTAssertEqual(-1, impl("", "a"))
        XCTAssertEqual(2, impl("123123123", "3"))
        XCTAssertEqual(2, impl("123123123", "312"))
        XCTAssertEqual(-1, impl("123123123", "4"))
        XCTAssertEqual(-1, impl("123", "1234"))
        XCTAssertEqual(-1, impl("mississippi", "issipi"))
    }
}

